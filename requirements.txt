Django
djangorestframework
Markdown
django-filter
#mysqlclient==1.3.7
psycopg2-binary
gunicorn

# develop
coverage
django-extensions
